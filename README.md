# CreatePDF
Create pdf with drawn lines, pictures, links and up to 128 different utf-8 chars build in vanila python (also has Tkinter GUI).

Examples:
![examples/file.pdf](examples/file.pdf)

<img src="examples/ScreenRecorderProject3.gif"  width="400" height="350"> => <img src="examples/gifpdf.jpg"  width="400" height="400">


# Usage
## pip install from test.pypi
[https://test.pypi.org/project/createPDF/](https://test.pypi.org/project/createPDF/)

## manual usage
1. Download project [https://gitlab.com/Beetsaber/createPDF/-/archive/main/createPDF-main.zip](https://gitlab.com/Beetsaber/createPDF/-/archive/main/createPDF-main.zip).
2. Extract it.
3. Run createPDF/createPDF.py or createPDF/gui.py.

Use Verdana for utf-8 characters otherwise use standard 14 fonts from _createPDF.py_.

Examples of adding images and url links are given in _createPDF.py_ `getPagesList()` functions.

Line drawing can be done via _gui.py_ Brush button.

## gui.py
Running gui.py will run Tkinter GUI. F1 for instructions. F4 will create _log.py_ file so afterwards modification can be made (some features are not implemented in gui.py)

## log.py basic Examples
`pLst = [Page(), Page(),....]` \
`Page(  [Text(),Text(),...], [Annot(),Annot(),...], [Line(),Line(),...], [Img(),Img(),...], [w,h]  )`
```python
#!/usr/bin/python
# -*- coding: utf-8 -*-

from createPDF import *

pLst = []
pLst.append(  Page( [Text('Courier', 12, 'latin-1 Text in red color', 150, 435, '1.00 0.00 0.00'), \
		     Text('Verdana', 20, 'utf-8:  Éé, Èè, Êê, Ëë Ññ', 150, 500, '0.00 0.00 0.00')],  ),  )

createPDF('f.pdf', 700, 600, pLst)
```
```python
#!/usr/bin/python
# -*- coding: utf-8 -*-

from createPDF import *

pLst = []
pLst.append(  Page( [Text('Courier', 12, 'latin-1 Text in blue color', 150, 435, '0.00 0.00 0.87')], [],  )  )
pLst.append(  Page( [Text('Verdana', 20, 'utf-8:  Éé, Èè, Êê, Ëë Ññ', 150, 500, '0.00 0.00 0.00')], [],  )  )

createPDF('f.pdf', 700, 600, pLst)
```

## batch files into pdf
Use `batch_join_txt_and_jpg_to_1_pdf.py` file to gather all .txt and .jpg files and create 1 single large pdf with all those file within it.

# License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License v3.0 as attached.

“The GNU Affero General Public License requires the operator of a network server to provide the source code of the modified version running there to the users of that server. Therefore, public use of a modified version, on a publicly accessible server, gives the public access to the source code of the modified version.”

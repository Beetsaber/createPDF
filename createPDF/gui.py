from createPDF import *

import tkinter as tk# from Tkinter import tk for py2
from tkinter.colorchooser import askcolor
from tkinter.filedialog import askopenfilename# import tkFileDialog for py2
from PIL import Image, ImageTk
from tkinter import messagebox as mb

# because tkinter has Text object it overwrites one from createPDF.py(i.e. objectsWriting.py)
from createPDF import Text as T

class Paint(object):

	helpText = 'Press "Chose text position". Click on screen.\nEnter text in left box. Press "Add text" or "Add link" or "Add image"\nNote: "Add link" will make a prompt for url in command line(cmd).\nAdjust text/link size with top tools.\nPress "Add page to PDF". Press "Create PDF".\nNote: Ctrl will set position as bottom left corner.\nNote: Adding link with blank textbox will add 100x15 link rectangle.\nNote: F4 to toggle logging output.'
	LOG_OUTPUT_TO_FILE = False# don't change, controlled by turnOnOfflogging which is keymapped to F4
	GRID = False
	GRID_SIZE = 80

	DEFAULT_COLOR = 'black'
	LINK_BLUE = '#1A0DAB'
	
	#['Times-Roman', 'Helvetica', 'Courier', 'Symbol', 'Times-Bold', 'Helvetica-Bold',
	#'Courier-Bold', 'ZapfDingbats', 'Times-Italic', 'Helvetica-Oblique', 'Courier-Oblique',
	#'Times-BoldItalic', 'Helvetica-BoldOblique', 'Courier-BoldOblique']
	fontName = 'Courier'
	FONT_W = 1.54# width of font in relation to its height
	
	BLANK_LINK_W = 100
	BLANK_LINK_H = 15
	
	# these magic numbers might not work on different os-s, if there are offset problems set them to 1
	W_OFF = 1.4
	W_OFF_IMG = 1.45
	
	#self.added_img_w, self.added_img_h #used for scalling Page to last added image size

	def __init__(self):
		print(self.helpText)
		
		self.pagesW, self.pagesH = 700, 600
		
		self.root = tk.Tk()
		
		self.scaleImgToPage_checkboxVar = tk.IntVar()
		self.scale_img_checkbox = tk.Checkbutton(self.root, text="scale img to page", variable=self.scaleImgToPage_checkboxVar)
		self.scale_img_checkbox.grid(row=0, column=0)

		self.brush_button = tk.Button(self.root, text='Brush', command=self.use_brush)
		self.brush_button.grid(row=0, column=1)

		self.color_button = tk.Button(self.root, text='Color', command=self.choose_color)
		self.color_button.grid(row=0, column=2)

		self.eraser_button = tk.Button(self.root, text='Eraser', command=self.use_eraser)
		self.eraser_button.grid(row=0, column=3)

		self.choose_size_button = tk.Scale(self.root, from_=1, to=60, orient=tk.HORIZONTAL)
		self.choose_size_button.grid(row=0, column=4)

		self.grid_toggle_button = tk.Button(self.root, text='Grid on/off', command=self.toggle_grid)
		self.grid_toggle_button.grid(row=0, column=5)

		self.c = tk.Canvas(self.root, bg='white', width=self.pagesW, height=self.pagesH)
		self.c.grid(row=1, column=0, columnspan=7)
		
		self.t = tk.Text(self.root, width=42, height=6)
		self.t.grid(row=2, column=0, rowspan=4, columnspan=4, padx=6, pady=8)

		self.text_position_button = tk.Button(self.root, text='Chose text(image bottom left corner) position', command=self.use_text_position)
		self.text_position_button.grid(row=2, column=4, columnspan=3, padx=2, pady=2, sticky=tk.NSEW)
		
		self.add_text_button = tk.Button(self.root, text='Add text to page', command=self.add_text)
		self.add_text_button.grid(row=3, column=4, padx=2, pady=2)
		
		self.add_link_button = tk.Button(self.root, text='Add link', command=self.add_link)
		self.add_link_button.grid(row=3, column=5, padx=2, pady=2)
		
		self.add_image_button = tk.Button(self.root, text='Add image', command=self.add_image)
		self.add_image_button.grid(row=3, column=6, padx=2, pady=2)
		
		self.add_page_button = tk.Button(self.root, text='Add page to PDF', command=self.add_page)
		self.add_page_button.grid(row=4, column=4, columnspan=1, padx=2, pady=2, sticky=tk.NSEW)
		
		self.add_page_cropped_button = tk.Button(self.root, text='Add page of last image size to PDF', command=self.add_page_cropped_to_last_img)
		self.add_page_cropped_button.grid(row=4, column=5, columnspan=2, padx=2, pady=2, sticky=tk.NSEW)
		
		self.create_pdf_button = tk.Button(self.root, text='Create PDF', command=self.createPdf)
		self.create_pdf_button.grid(row=5, column=4, columnspan=3, padx=2, pady=2, sticky=tk.NSEW)

		self.pages = []
		
		self.pageTxts = []
		self.pageAnnots = []
		self.pageLines = []
		self.pageImgs = []
		
		self.added_img_w, self.added_img_h = self.pagesW, self.pagesH
		
		self.setup()
		self.root.mainloop()
		
	def assignButtons(self):
		self.c.bind('<B1-Motion>', self.paint)
		self.c.bind('<ButtonRelease-1>', self.reset)
		self.c.bind('<Button-1>', self.store_label_xy)
		self.c.bind('<Button-3>', self.cords)#right click
		self.c.bind('<F4>', self.turnOnOfflogging)
		self.c.bind('<F1>', self.help)
		self.c.bind('<Escape>', self.help)
		self.c.bind('<Control_L>', self.clearPosition)
		self.c.focus_set()

	def setup(self):
		self.old_x = None
		self.old_y = None
		self.choose_size_button.set(12)
		self.line_width = self.choose_size_button.get()
		self.color = self.DEFAULT_COLOR
		
		self.active_button = self.brush_button
		self.activate_button(self.brush_button)
		
		self.assignButtons()
		
		self.label_x = None
		self.label_y = None

	def use_text_position(self):
		self.activate_button(self.text_position_button)
		
	def add_text(self):
		
		# no text in textbox or no position selected
		if len(self.t.get('0.0', tk.END).strip()) < 1 or self.label_x == None or self.label_x == None: return
		
		self.activate_button(self.add_text_button)
		
		if self.label_x and self.label_x:
			
			txtSize = self.choose_size_button.get()
			
			l = tk.Label(self.c, font=(self.fontName, int(txtSize/self.W_OFF)), text = self.t.get('0.0', tk.END), fg=self.color, anchor='w', justify='left') 
			l.place(x=self.label_x, y=self.label_y)
			
			colorFloats = self.rgbColorTo_0_1_range(self.color)
			# make sure '\n' in textbox are translated into pdf
			nxtLine = 0
			for line in self.t.get('0.0', tk.END).strip().split('\n'):
				line = line.replace("'", "\\'")
				txt = T(self.fontName, txtSize, line, self.label_x, self.pagesH-self.label_y-8-nxtLine, colorFloats)
				self.pageTxts.append(txt)# save to later pass to Page()
				nxtLine += txtSize
			
		self.activate_button(self.text_position_button)
				
	def add_link(self):
		
		# no position selected
		if self.label_x == None or self.label_x == None: return
		
		self.activate_button(self.add_link_button)
		
		if self.label_x and self.label_x:
			
			txtSize = self.choose_size_button.get()
			
			l = tk.Label(self.c, font=(self.fontName, int(txtSize/self.W_OFF)), text = self.t.get('0.0', tk.END), fg=self.LINK_BLUE, anchor='w', justify='left') 
			l.place(x=self.label_x, y=self.label_y)
			
			colorFloats = self.rgbColorTo_0_1_range(self.LINK_BLUE)
			# make sure '\n' in textbox are translated into pdf
			nxtLine = 0
			for line in self.t.get('0.0', tk.END).strip().split('\n'):
				line = line.replace("'", "\\'")
				txt = T(self.fontName, txtSize, line, self.label_x, self.pagesH-self.label_y-8-nxtLine, colorFloats)
				
				url = ''
				while True:
					try:
						url = input('Enter website URL (must be valid):')
					except (KeyboardInterrupt, RuntimeError): pass
					if len(url) < 4: continue
					if url[:9] != 'https://w' and url[:8] != 'http://w' and url[:4] != 'www.': continue
					break
				
				if len(self.t.get('0.0', tk.END).strip()) < 1: # no text in textbox
					a = Annot(url, self.label_x, self.pagesH-self.label_y-8-self.BLANK_LINK_H, self.label_x+self.BLANK_LINK_W, self.pagesH-self.label_y)
				else:
					self.pageTxts.append(txt)# save to later pass to Page()
					a = Annot(url, self.label_x, self.pagesH-self.label_y-8-int(txtSize/2)-nxtLine, self.label_x+int(txtSize/self.FONT_W)*len(line), self.pagesH-self.label_y-8-nxtLine+txtSize)
				self.pageAnnots.append(a)
				
				nxtLine += txtSize
			
		self.activate_button(self.text_position_button)
		
	def add_image(self):
		filename = askopenfilename(title = "Select image file to copy into pdf page")
		if self.label_x == None or self.label_y == None: self.label_x, self.label_y = 0, self.pagesH
		
		im = None
		try:
			im = Image.open(filename)
		except AttributeError: return
		
		l = tk.Label(self.c, image = ImageTk.PhotoImage(im), anchor='w', justify='left')
		l.place(x=self.label_x, y=self.label_y-int(self.pagesH/self.W_OFF_IMG))
		
		if self.scaleImgToPage_checkboxVar.get() == 1:
			#FIXME bug if image is being scaled when self.label_x and self.label_y are not 0,0 (reason for bug might be use of W_OFF_IMG)
			scaleX = self.pagesW/(im.size[0]+self.label_x)
			scaleY = self.pagesH/(im.size[1]+self.pagesH-self.label_y)
			self.pageImgs.append( Img(filename, self.label_x, self.pagesH-self.label_y, im.size[0], im.size[1], scaleX, scaleY) )# Note: image drawn from bottom left image corner
		else:
			self.pageImgs.append( Img(filename, self.label_x, self.pagesH-self.label_y, im.size[0], im.size[1]) )# Note: image drawn from bottom left image corner
			self.added_img_w, self.added_img_h = im.size[0], im.size[1]
		
	def use_brush(self):
		self.activate_button(self.brush_button)

	def choose_color(self):
		self.eraser_on = False
		self.color = askcolor(color=self.color)[1]
		if self.color == None: self.color = 'black'

	def use_eraser(self):
		self.activate_button(self.eraser_button, eraser_mode=True)

	def activate_button(self, some_button, eraser_mode=False):
		self.active_button.config(relief=tk.RAISED)
		some_button.config(relief=tk.SUNKEN)
		self.active_button = some_button
		self.eraser_on = eraser_mode
		
	def toggle_grid(self):
		self.GRID = not self.GRID
		if self.GRID: self.grid_toggle_button.config(relief=tk.SUNKEN)
		else: self.grid_toggle_button.config(relief=tk.RAISED)

	def rgbColorTo_0_1_range(self, paint_color):
		# translate paint_color(ex. '#AAFFBB', 'white', 'black') to rgb-s in range 0.00-1.00(ex. '0.80 1.00 1.87')
		if paint_color == None: return '0.00 0.00 0.00'
		
		if paint_color == 'white':
			paint_color = '#FFFFFF'
		if paint_color == 'black':
			paint_color = '#000000'
			
		if paint_color[0] == '#':
			paint_color = paint_color[1:]
		
		colorFloats = '{:.2f}'.format(int(paint_color[:2], 16)*(1/255.))+' {:.2f}'.format(int(paint_color[2:4], 16)*(1/255.))+' {:.2f}'.format(int(paint_color[4:6], 16)*(1/255.))
		return colorFloats

	def paint(self, event):
		
		# no need to draw line if self.text_position_button is pressed
		if self.text_position_button and self.text_position_button['relief'] == 'sunken': return
		
		self.line_width = self.choose_size_button.get()
		paint_color = 'white' if self.eraser_on else self.color
		if self.old_x and self.old_y:
			self.c.create_line(self.old_x, self.old_y, event.x, event.y,
							   width=self.line_width, fill=paint_color,
							   capstyle=tk.ROUND, smooth=tk.TRUE, splinesteps=36)
			
			colorFloats = self.rgbColorTo_0_1_range(paint_color)
			
			self.pageLines.append(Line(self.line_width, self.old_x, self.pagesH-self.old_y-8, event.x, self.pagesH-event.y-8, colorFloats))# save to later pass to Page()
			
		self.old_x = event.x
		self.old_y = event.y

	def add_page(self, size=[]):
		p = Page( self.pageTxts, self.pageAnnots, self.pageLines, self.pageImgs, size )
		self.pages.append( p )
		print('Page added:', p)
		if self.LOG_OUTPUT_TO_FILE: self.logToFile('pLst.append(  '+str(p)+'  )')
		
		self.pageTxts = []
		self.pageAnnots = []
		self.pageLines = []
		self.pageImgs = []
		
		self.c.delete("all")# does not always seem to work
		self.c = tk.Canvas(self.root, bg='white', width=self.pagesW, height=self.pagesH)
		self.assignButtons()
		
		self.c.grid(row=1, column=0, columnspan=7)
		self.c.update_idletasks()
		
	def add_page_cropped_to_last_img(self): self.add_page([self.added_img_w, self.added_img_h])
		
	def createPdf(self):
		pdfName = 'f.pdf'
		createPDF(pdfName, self.pagesW, self.pagesH, self.pages)
		print(pdfName + ' PDF created.')
		if self.LOG_OUTPUT_TO_FILE: self.logToFile('\ncreatePDF(\''+pdfName+'\', '+str(self.pagesW)+', '+str(self.pagesH)+', pLst)')
		else: print('Press F4 in gui to create log file with creation code.')
		
	def turnOnOfflogging(self, event):
		self.LOG_OUTPUT_TO_FILE = not self.LOG_OUTPUT_TO_FILE
		if self.LOG_OUTPUT_TO_FILE:
			with open('log.py', 'w', encoding='utf-8') as file:
				file.write('#!/usr/bin/python\n# -*- coding: utf-8 -*-\nfrom createPDF import *\n\npLst = []\n')
	def logToFile(self, txt):
		with open('log.py', 'a', encoding='utf-8') as file:
			file.write(str(txt)+'\n')
		
	def cords(self, event):
		mb.showinfo('Cords:', 'x=' + str(event.x) + ', y=' + str(event.y))
		
	def help(self, event):
		mb.showinfo('Info', self.helpText)
		
	def clearPosition(self, event):
		self.label_x, self.label_y = None, None
		
	def reset(self, event):
		self.old_x, self.old_y = None, None
		
	def store_label_xy(self, event):
		self.label_x, self.label_y = event.x, event.y
		if self.GRID:
			print(self.label_x, self.label_y)
			self.label_x, self.label_y = self.GRID_SIZE*(self.label_x//self.GRID_SIZE)+int(self.GRID_SIZE/2), self.GRID_SIZE*(self.label_y//self.GRID_SIZE)+int(self.GRID_SIZE/2)
			print('x=' + str(self.label_x) + ' y=' + str(self.label_y))


if __name__ == '__main__':
	Paint()
#!/usr/bin/python
# -*- coding: utf-8 -*-
from createPDF import *

pLst = []
pLst.append(  Page( [Text('Courier', 12, 'latin-1 Text in red color', 150, 435, '0.00 0.00 0.80'), \
					Text('Verdana', 20, 'utf-8:  Éé, Èè, Êê, Ëë Ññ', 150, 500, '0.00 0.00 0.00')],  )  )

createPDF('f.pdf', 700, 600, pLst)

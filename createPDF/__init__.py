import os
import sys
from os.path import dirname

import createPDF

# add createPDF package to sys.path
sys.path.append(dirname(createPDF.__file__))
#!/usr/bin/python
# -*- coding: utf-8 -*-
from createPDF import *

from os.path import isdir

import re
import codecs
	
from os import listdir, getcwd
from os.path import join, isfile

files_folder_name = 'files'
print('This script will create 1 pdf from all .txt files and .jpg images in adjacent folder called `' + files_folder_name + '`')
if isdir(files_folder_name): pass
else:
	print('\nThere is no folder named', files_folder_name, 'in', getcwd())
	raise SystemExit(0)


X_SIZE_PAGE, Y_SIZE_PAGE = 700, 600# change here
FONT_SIZE = 4# set inside getAsPage() not here
LINE_MAX_CHARS = 4# automatically calculated in getAsPage()


ESCAPE_SEQUENCE_RE = re.compile(r'''
    ( \\U........      # 8-digit hex escapes
    | \\u....          # 4-digit hex escapes
    | \\x..            # 2-digit hex escapes
    | \\[0-7]{1,3}     # Octal escapes
    | \\N\{[^}]+\}     # Unicode characters by name
    | \\[\\'"abfnrtv]  # Single-character escapes
    )''', re.UNICODE | re.VERBOSE)
def decode_escapes(s):
	def decode_match(match):
		try:
			return codecs.decode(match.group(0), 'unicode-escape')
		except UnicodeDecodeError:
			# In case we matched the wrong thing after a double-backslash
			return match.group(0)

	return ESCAPE_SEQUENCE_RE.sub(decode_match, s)
	
def split_str(seq, chunk, skip_tail=False):
	lst = []
	if chunk <= len(seq):
		lst.extend([seq[:chunk]])
		lst.extend(split_str(seq[chunk:], chunk, skip_tail))
	elif not skip_tail and seq:
		lst.extend([seq])
	return lst

def getLocationAndContentsOfFiles(extension, ignoreData=False):

	files = []
	path = getcwd()+'\\' + files_folder_name  # `files` folder location
	for filename in listdir(path):  # iterates over all the files in 'path'
		full_path = join(path, filename)  # joins the path with the filename
		if isfile(full_path):  # validate that it is a file
			if full_path.endswith(extension):  # open only text files
				with open(full_path) as f:  # open the file
					print(full_path)
					if ignoreData:
						files.append([full_path, None])
					else:
						files.append([full_path, f.read()])

	return files
	
def getAsPage(text, annotText, imagePath=''):
	global FONT_SIZE
	global LINE_MAX_CHARS
	
	a1 = Annot('ftp:///'+repr(annotText)[1:-1], 0, 0, int(X_SIZE_PAGE/4), int(Y_SIZE_PAGE/4))# Note if file name has : it will not be saved
	
	imageWidth, imageHeight = X_SIZE_PAGE, Y_SIZE_PAGE
	if imagePath != '': return Page( [], [a1,], [], [Img(imagePath, 0, 0, imageWidth, imageHeight)] )
	
	tLst = []
	fontSize = 18
	FONT_SIZE = max(fontSize, FONT_SIZE)
	LINE_MAX_CHARS = max(2*int(X_SIZE_PAGE/(fontSize+int(fontSize/(fontSize/5)))), LINE_MAX_CHARS)
	yInx = 0
	for s in split_str(text, LINE_MAX_CHARS):
		
		s = decode_escapes(repr(s)[1:-1])
		s = repr(s)[1:-1]
		
		xStartMargin, yStartMargin = int(X_SIZE_PAGE*.03), int(Y_SIZE_PAGE*.05)
		t1 = Text('Verdana', fontSize, s, xStartMargin, Y_SIZE_PAGE-yStartMargin-yInx, '0.00 0.00 0.00')
		
		tLst.append(t1)
		
		yInx += fontSize+5
	
	
	page = Page( tLst, [a1,], [], [] )
	
	return page

pLst = []

# SAVE ALL .txt FILES
locationAndText = getLocationAndContentsOfFiles('.txt')

getAsPage('','')# update global values for FONT_SIZE, LINE_MAX_CHARS

for location, text in locationAndText:
	
	for t in split_str(text, max(30-FONT_SIZE, 1)*LINE_MAX_CHARS):
		print('adding page:')
		pLst.append(getAsPage(t, location))


# SAVE ALL .jpg FILES
locationAndImageData = getLocationAndContentsOfFiles('.jpg', True)

for locationOfImage, _ in locationAndImageData:
	
	print('adding page:')
	pLst.append(getAsPage(None, locationOfImage, locationOfImage))


# Use batch command to list all txt files and save their paths into o.txt: dir /a:-d /o > o.txt
createPDF('f.pdf', X_SIZE_PAGE, Y_SIZE_PAGE, pLst)

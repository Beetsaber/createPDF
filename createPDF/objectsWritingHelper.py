from writeToFile import writeToFile, writeToFileBytes, endCharLength

class WriteableObject():
	def __init__(self, fp):
		self.fp = fp

	# return value should be size of written data (ie. size of object)
	# note: this doesnt garantee that return value will be of type int
	def writeObject(self, num) -> int:
		raise NotImplementedError('WriteableObject must be overwritten by one of its implementations')
	
class WriteFreeObject(WriteableObject):

	def writeObject(self, num):
		return 0
		
		
class WriteCatalogObject(WriteableObject):

	def __init__(self, fp, pagesIndex):
		super().__init__(fp)
		self.pagesIndex = pagesIndex
	
	def writeObject(self, num):
		revision = 0
		w = writeToFile(self.fp, str(num)+' '+str(revision)+' '+'obj')
		w += writeToFile(self.fp, '<< /Type /Catalog')
		w += writeToFile(self.fp, '/Pages '+str(self.pagesIndex)+' 0 R')#, '\r'
		w += writeToFile(self.fp, '>>')
		w += writeToFile(self.fp, 'endobj')
		
		writtenSize = w
		return writtenSize
		

class WritePagesObject(WriteableObject):

	def __init__(self, fp, kidsIndex, sizeW, sizeH):
		super().__init__(fp)
		self.kidsIndex = kidsIndex
		self.sizeW, self.sizeH = sizeW, sizeH
	
	def writeObject(self, num):
		revision = 0
		w = writeToFile(self.fp, str(num)+' '+str(revision)+' '+'obj')
		w += writeToFile(self.fp, '<< /Type /Pages')
		w += writeToFile(self.fp, '/Kids ['+''.join(str(i)+' 0 R\n' for i in self.kidsIndex))
		w += writeToFile(self.fp, ']')
		w += writeToFile(self.fp, '/Count '+str( len(self.kidsIndex) ))
		w += writeToFile(self.fp, '/MediaBox [0 0 '+str(self.sizeW)+' '+str(self.sizeH)+']')
		w += writeToFile(self.fp, '>>')
		w += writeToFile(self.fp, 'endobj')
		
		writtenSize = w
		return writtenSize
	
	
class WritePageObject(WriteableObject):

	def __init__(self, fp, parentIndex, contentStreamIndex, fontLst, firstAnnotIndex, numberOfAnnots, firstXObjectIndex, numberOfXObjects, pdfRefName, size):
		super().__init__(fp)
		self.parentIndex = parentIndex
		self.contentStreamIndex = contentStreamIndex
		self.fontLst = fontLst
		self.firstAnnotIndex = firstAnnotIndex
		self.numberOfAnnots = numberOfAnnots
		self.firstXObjectIndex = firstXObjectIndex
		self.numberOfXObjects = numberOfXObjects
		self.pdfRefName = pdfRefName
		self.size = size
	
	# this is example of indirect referencing of font (F13 holds the font details)
	#/Resources'\n'<<  /Font  <<  /F13 23 0 R  >>>>
	def writeObject(self, num):
		revision = 0
		w = writeToFile(self.fp, str(num)+' '+str(revision)+' '+'obj')
		w += writeToFile(self.fp, '<< /Type /Page')
		w += writeToFile(self.fp, '/Parent '+str(self.parentIndex)+' 0 R')
		
		
		w += writeToFile(self.fp, '/Resources')
		
		w += writeToFile(self.fp, '<< /Font')
		w += writeToFile(self.fp, '<< ')
		
		for i, fontIndex in enumerate(self.fontLst):
			w += writeToFile(self.fp, '/F'+str(fontIndex)+' '+str(fontIndex)+' 0 R')#str(i+1)
		
		w += writeToFile(self.fp, '>>')# close Font
		
		if self.numberOfXObjects == 0: pass
		else:
			w += writeToFile(self.fp, '/XObject << '+ '\n'.join([ '/'+self.pdfRefName+str(i+1)+' '+str(inx)+' 0 R' for i, inx in enumerate(range(self.firstXObjectIndex, self.firstXObjectIndex+self.numberOfXObjects)) ]) +'\n>>')
		
		w += writeToFile(self.fp, '>>')# close Resources
		
		
		if self.size != []: w += writeToFile(self.fp, '/MediaBox [0 0 '+str(self.size[0])+' '+str(self.size[1])+']')# change page size via MediaBox
		# if no size is given MediaBox is inherited from Pages object
		
		
		w += writeToFile(self.fp, '/Contents '+str(self.contentStreamIndex)+' 0 R')
		
		if self.numberOfAnnots == 0: pass
		else:
			w += writeToFile(self.fp, '/Annots [ '+ '\n'.join([ str(i)+' 0 R' for i in range(self.firstAnnotIndex, self.firstAnnotIndex+self.numberOfAnnots) ]) +'\n]')
		
		w += writeToFile(self.fp, '>>')
		w += writeToFile(self.fp, 'endobj')
		
		writtenSize = w
		return writtenSize
		
		
class WriteFontObject(WriteableObject):

	standard14Fonts = ['Times-Roman', 'Helvetica', 'Courier', 'Symbol', 'Times-Bold', 'Helvetica-Bold',
								'Courier-Bold', 'ZapfDingbats', 'Times-Italic', 'Helvetica-Oblique', 'Courier-Oblique',
								'Times-BoldItalic', 'Helvetica-BoldOblique', 'Courier-BoldOblique']
	#specalChars = []

	#if lstOfUTF8charsThatNeedToBeSubed != [] then Custom font will be used
	def __init__(self, fp, font, lstOfUTF8charsThatNeedToBeSubed=[]):
		super().__init__(fp)
		
		if lstOfUTF8charsThatNeedToBeSubed == [] and font != 'Verdana':
			self.specalChars = lstOfUTF8charsThatNeedToBeSubed
			assert font in self.standard14Fonts, 'Error: not one of 14 standard fonts'
			self.font = font
		else:
			#print('%using custom font because utf-8 codepoints in text')
			self.specalChars = lstOfUTF8charsThatNeedToBeSubed
			self.font = 'AAAAAA+Verdana'
	
	def writeObject(self, num):
		revision = 0
		
		w = writeToFile(self.fp, str(num)+' '+str(revision)+' '+'obj')
		
		if self.specalChars == []:
			w += writeToFile(self.fp, '<< /Type /Font')
			w += writeToFile(self.fp, '/Subtype /Type1')
			w += writeToFile(self.fp, '/BaseFont /'+self.font)
			w += writeToFile(self.fp, '>>')
			w += writeToFile(self.fp, 'endobj')

		else:
			print('%specalChars:' , self.specalChars)
			w += writeToFile(self.fp, '<< /Type /Font')
			w += writeToFile(self.fp, '/Subtype /TrueType')
			w += writeToFile(self.fp, '/BaseFont /'+self.font)
			w += writeToFile(self.fp, '/FirstChar 32')
			w += writeToFile(self.fp, '/LastChar 122')
			w += writeToFile(self.fp, '/Encoding /WinAnsiEncoding')
			w += writeToFile(self.fp, '/Widths [ '+ '600 ' * 91 + ']')
			w += writeToFile(self.fp, '/FontDescriptor << /Type /FontDescriptor /FontName /'+self.font+' /FontWeight 400 /Flags 32 /FontBBox[ -560 -207 1523 765 ] /ItalicAngle 0 /Ascent 1005 /Descent -207 /CapHeight 765 /XHeight 250 /StemV 50 /AvgWidth 508 /MaxWidth 2083 /MissingWidth 600 >>')
			
			#AGLglyphlist glyphs at https://github.com/adobe-type-tools/agl-aglfn/blob/master/glyphlist.txt
			AGLglyphlist_fileName = 'AGLglyphlist.txt'
			AGLglyphlist = []
			try:
				AGLglyphlist = open(AGLglyphlist_fileName, 'r').readlines()
			except FileNotFoundError:
				import os
				dir = os.path.dirname(__file__)
				AGLglyphlist = open(os.path.join(dir, AGLglyphlist_fileName), 'r').readlines()
			
			d = ' '
			for c, cHexInFile in zip(self.specalChars, range(128, 128+len(self.specalChars))):
			
				for line in AGLglyphlist: 
					replaceGlyph, unicodeCodepoint = line.strip().split(';')
					if len(unicodeCodepoint) > 4: continue# skip glyphs with too many codepoints
					if int(unicodeCodepoint, 16) <= int('0xA0', 16): continue# skip glyphs which fit within ASCII and unicode C1 controls block
					if hex(int(unicodeCodepoint, 16)) == '0x'+hex(ord(c))[2:]:
						d += str(cHexInFile) + ' /'+replaceGlyph + ' '
						break# there are multiple coresponding codepoints so add only first one
					
			w += writeToFile(self.fp, '/Encoding << /Type /Encoding /BaseEncoding /WinAnsiEncoding /Differences ['+d+'] >>')#BaseEncoding WinAnsiEncoding
			#NOTE: Differences is allowed for TrueType due to 9.6.6.4 Encodings for TrueType Fonts from iso 32000:
			#"If the Encoding entry is a dictionary, the table shall be initialized with the entries from the dictionary’s
			#BaseEncoding entry (see Table 114). Any entries in the Differences array shall be used to update the table."
			w += writeToFile(self.fp, '>>')
			w += writeToFile(self.fp, 'endobj')
		
		writtenSize = w
		return writtenSize
		

class WriteTextObject(WriteableObject):
	
	def __init__(self, fp, textLst, lineLst, imgLst):
		super().__init__(fp)
		assert isinstance(textLst, list), 'textLst must be of type list, and must contain tuples with ( fontInx, Text() ); (note:list may be empty)'
		assert isinstance(lineLst, list), 'lineLst must be of type list, and must contain instances of Line(); (note:list may be empty)'
		assert isinstance(imgLst, list), 'imgLst must be of type list, and must contain instances of Img(); (note:list may be empty)'
		self.textLst = textLst
		self.lineLst = lineLst
		self.imgLst = imgLst
	
	def writeObject(self, num):
		revision = 0
		w = writeToFile(self.fp, str(num)+' '+str(revision)+' '+'obj')
		
		
		# CALC LENGTHS FOR TEXTs, LINEs AND IMGs
		
		length = 0# from stream to endstream keywords (without endChar after stream and endChar before endstream)
		for fontInx, t in self.textLst:
			size, text, x, y, color = t.size, t.text, t.x, t.y, t.color
			# *6 because endChar-s for 6 lines between stream and endstream
			length += len('BT'+color+' rg'+'/F'+str(fontInx)+' '+str(size)+' Tf'+str(x)+' '+str(y)+' Td'+'('+text+') Tj'+'ET') + endCharLength()*6
		length -= endCharLength()# -1 because endChar before endstream is not counted
		
		for i, img in enumerate(self.imgLst):
			refName = img.pdfRefName# pdfRefName should be statically referenced, but cant be because Img is not accessible
			length += len('q'+str(img.w)+' 0 0 '+str(img.h)+' '+str(img.x)+' '+str(img.y)+' cm'+str(img.scaleW)+' 0 0 '+str(img.scaleH)+' 0 0 cm'+'/'+refName+str(i+1)+' Do'+'Q')+4# +4 because 4 lines
		
		if self.lineLst != []: length += len('1 J')
		for line in self.lineLst:
			length += len(line.color+' RG')# rgb stroking color
			length += len(str(line.width)+' w')
			length += len(str(line.x1)+' '+str(line.y1)+' m')
			length += len(str(line.x2)+' '+str(line.y2)+' l')
			length += len('S')
		
		
		# ADD IMAGES
		
		if length < 0: length = 0
		w += writeToFile(self.fp, '<< /Length '+ str(length) +' >>')
		w += writeToFile(self.fp, 'stream')# breaking line char after 'stream' is not counted towards <</Length>>
		
		for i, img in enumerate(self.imgLst):
			w += writeToFile(self.fp, 'q')
			#w 0 0 h x y cm;  w h will scale image.  roatate: cosq  sinq  -sinq  cosq  0  0.  skew: 1  tana  tanb  1  0  0
			w += writeToFile(self.fp, str(img.w)+' 0 0 '+str(img.h)+' '+str(img.x)+' '+str(img.y)+' cm')
			w += writeToFile(self.fp, str(img.scaleW)+' 0 0 '+str(img.scaleH)+' 0 0 cm')
			w += writeToFile(self.fp, '/'+refName+str(i+1)+' Do')
			w += writeToFile(self.fp, 'Q')
		
		
		# ADD LINES
		
		if self.lineLst != []:
			w += writeToFile(self.fp, '1 J')# '1 J' is for Round line end, '0 J' is for Butt cap (flat) end
		
		for line in self.lineLst:
			w += writeToFile(self.fp, line.color+' RG')# rgb stroking color
			w += writeToFile(self.fp, str(line.width)+' w')
			w += writeToFile(self.fp, str(line.x1)+' '+str(line.y1)+' m')
			w += writeToFile(self.fp, str(line.x2)+' '+str(line.y2)+' l')
			w += writeToFile(self.fp, 'S')
		
		
		# ADD TEXT
		
		for fontInx, t in self.textLst:
			size, text, x, y, color = t.size, t.text, t.x, t.y, t.color
			
			# Note: make sure to not change lines bellow without also changing length calculation
			w += writeToFile(self.fp, 'BT')
			w += writeToFile(self.fp, color+' rg')# rgb nonstroking color
			w += writeToFile(self.fp, '/F'+str(fontInx)+' '+str(size)+' Tf')
			w += writeToFile(self.fp, str(x)+' '+str(y)+' Td')
			w += writeToFile(self.fp, '('+text+') Tj')
			w += writeToFile(self.fp, 'ET')
		
		w += writeToFile(self.fp, 'endstream')# breaking line char on previous line in not counted towards <</Length>>
		w += writeToFile(self.fp, 'endobj')
		
		writtenSize = w
		return writtenSize
		

class WriteAnnotObject(WriteableObject):

	def __init__(self, fp, annot):
		super().__init__(fp)
		self.url = annot.url
		self.coordinates = annot.coordinates
		
	def writeObject(self, num):
		revision = 0
		w = writeToFile(self.fp, str(num)+' '+str(revision)+' '+'obj')
		
		# after /Annot possible to add:
		# /Border[horizontalRoundingOfEdge verticalRoundingOfEdge borderWidth] (ex. /Border[10 10 1])
		# in order to get black border around link or give rounded shape to link
		w += writeToFile(self.fp, '<< /Type /Annot /Subtype /Link /Rect [ '+self.coordinates+' ]')
		w += writeToFile(self.fp, '/A << /Type /Action /S /URI /URI ('+self.url+') >>')
		w += writeToFile(self.fp, '>>')
		
		w += writeToFile(self.fp, 'endobj')
		
		writtenSize = w
		return writtenSize
		
		
class WriteXObject(WriteableObject):

	def __init__(self, fp, img):
		super().__init__(fp)
		self.img = img
		
	def writeObject(self, num):
		revision = 0
		w = writeToFile(self.fp, str(num)+' '+str(revision)+' '+'obj')
		
		import os
		w += writeToFile(self.fp, '<< /Type /XObject /Subtype /Image /Width '+str(self.img.w)+' /Height '+str(self.img.h)+' /ColorSpace /DeviceRGB /BitsPerComponent 8 /Length '+str(os.stat(self.img.fileName).st_size)+' /Filter /DCTDecode >>')
		
		w += writeToFile(self.fp, 'stream')

		with open(self.img.fileName, 'rb') as f:
			inFileData = f.readlines()
			for row in inFileData:
				w += writeToFileBytes(self.fp, row, end='')

		w += writeToFile(self.fp, '')
		w += writeToFile(self.fp, 'endstream')

		w += writeToFile(self.fp, 'endobj')
		
		writtenSize = w
		return writtenSize


def objects(fp, headerSize, lst_of_objects):

	addresses_of_objs = []

	num = 1
	
	addrOfObj = headerSize# how far from start of the file has been written
	sizeWritten = 0
	for o in lst_of_objects:
		if isinstance(o, WriteableObject):
		
			if isinstance(o, WriteFreeObject):
				addresses_of_objs.append(addrOfObj)
				continue
		
			sizeWritten = o.writeObject(num)
			addresses_of_objs.append(addrOfObj)
			
			# next o
			num += 1
			addrOfObj += sizeWritten
			
		else:
			print('%Warning: object is not of type WriteableObject; Skipped adding it to the file.')
	
	# if writting another object add previously written object size to addresses_of_objs as that would be address of new object
	
	last_object_size = sizeWritten
	return addresses_of_objs, last_object_size

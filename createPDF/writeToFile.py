'''
Any of the following may be used to end line depending on os/reader implementation:

    0x0a "line feed"
    0x0d "carriage return" (not allowed as the EOL directly after the stream keyword)
    0x0d0a "CR + LF"
'''
# write to file and return size of written
# note: fp should be opened in binary mode
# note: end='\r' or '\t' or similar may not give correct sizeWritten
# note: if encoding='utf-8' of fp for chars >128 writeToFile may return wrong size (unless chars substituted in font)
# note: function imported in objectsWritting and referencesWritting
# function works with strings
def writeToFile(fp, txt, end='\r\n'):
	import io
	assert isinstance(fp, io.IOBase) or isinstance(fp, io.TextIOWrapper), 'fp passed which is not io.IOBase or _io.TextIOWrapper'
	# TODO assert fp open as 'wb'
	
	# # to verbose switch on/off
	# .encode('utf-8') before printing in order to pipe to file
	print(str(txt.encode('utf-8'))+'..' if len(txt)>0 and txt[0]=='%' else txt+'..')#'..' represents \n\r (0x0D0A)
	
	sizeWritten = 0
	if len(txt)>0 and txt[0]=='%':
		sizeWritten = fp.write(bytes(txt, 'utf-8'))
	else: sizeWritten = fp.write(bytes(txt, 'latin-1'))
	
	if len(bytes(txt, 'latin-1')) != len(txt) and len(txt)>0 and txt[0]!='%':
		print('\n\n\nAdding extra chars to pdf to represent unicode characters:', bytes(txt, 'latin-1'), txt, '\n\n\n')
	
	sizeWritten += fp.write(bytes(end, 'utf-8'))
	
	return sizeWritten
	
# returns how many bytes at the end are written with every writeToFile call (assuming default end argument)
def endCharLength(): return 2# '\n'


# write to file and return size of written
# func used with fp open as 'w' not 'wb'
# function works with bytes
def writeToFileBytes(fp, txt, end=''):
	import io
	assert isinstance(fp, io.IOBase) or isinstance(fp, io.TextIOWrapper), 'fp passed which is not io.IOBase or _io.TextIOWrapper'
	# TODO assert fp open as 'w'
	
	sizeWritten = fp.write(txt)
	
	return sizeWritten
	
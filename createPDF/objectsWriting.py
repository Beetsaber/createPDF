from objectsWritingHelper import *

#stucture used to WriteTextObject-s
class Text():

	# color 0.0-1.0 (ex. color = '0.1 1.0 0.6' (giving blue))
	def __init__(self, fontName, size, text, x, y, color='0 0 0'):
		self.fontName = fontName
		self.size = size
		self.text = text# may be changed before WriteTextObject() if there are utf-8 codepoints within it by substitute chars
		self.x = x
		self.y = y
		self.color = color

	def __str__(self):
		return 'Text(\''+self.fontName+'\', '+str(self.size)+', \''+self.text+'\', '+str(self.x)+', '+str(self.y)+', \''+self.color+'\')'

#stucture used to WritePageObject-s
class Line():

	def __init__(self, width, x1, y1, x2, y2, color='0 0 0'):
		self.width = width
		self.x1, self.y1, self.x2, self.y2 = x1, y1, x2, y2
		self.color = color

	def __str__(self):
		return 'Line('+str(self.width)+', '+str(self.x1)+', '+str(self.y1)+', '+str(self.x2)+', '+str(self.y2)+', \''+self.color+'\')'

#stucture used to WritePageObject-s
class Annot():
	
	# coordinates = 'x1 y1 x2 y2' (forms clickable quadrilateral shape which opens url provided)
	
	def __init__(self, url, x1, y1, x2, y2):
		self.url = url
		self.coordinates = str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)
		self.x1, self.y1, self.x2, self.y2 = x1, y1, x2, y2# used for __str__
	
	def __str__(self):
		return 'Annot(\''+self.url+'\', '+str(self.x1)+', '+str(self.y1)+', '+str(self.x2)+', '+str(self.y2)+')'

class Img():

	pdfRefName = 'Im'# used as static variable to assure that pdf XObject and Do/q commands are identical when referencing written Img (ie. Im1, Im2,...)

	#w, h should be exact size of jpg (use scale to shrink/stretch)
	def __init__(self, fileName, x, y, w, h, scaleW=1, scaleH=1):
		self.fileName = fileName
		self.x, self.y, self.w, self.h = x, y, w, h
		self.scaleW, self.scaleH = round(scaleW, 2), round(scaleH, 2)
	
	def __str__(self):
		if self.scaleW==1 and self.scaleH==1:
			return 'Img(\''+self.fileName+'\', '+str(self.x)+', '+str(self.y)+', '+str(self.w)+', '+str(self.h)+')'
		else:
			return 'Img(\''+self.fileName+'\', '+str(self.x)+', '+str(self.y)+', '+str(self.w)+', '+str(self.h)+', '+str(self.scaleW)+', '+str(self.scaleH)+')'

class Page():

	#textLst = []#with Text() instances (where each Text has txt to be displayed and its font size,type,x,y...)
	#self.fonts = []#used across multiple Text elements
	#self.annots
	#self.lines
	#self.imgs
	#self.size = [w, h]#if omitted MediaBox property from Pages object will be used

	def __init__(self, textLst=[], annotLst=[], linesLst=[], imgLst=[], size=[]):
		assert isinstance(textLst, list), 'Error in Page() constuctor: textLst must be of type list, and must contain instances of Text() objects'
		for i in textLst:
			assert isinstance(i, Text), 'Error in Page() constuctor: textLst has element which is not of type Text, but of type:'+str(type(i))
		
		assert isinstance(annotLst, list), 'Error in Page() constuctor: annotLst must be of type list, and must contain instances of Annot() objects'
		for i in annotLst:
			assert isinstance(i, Annot), 'Error in Page() constuctor: annotLst has element which is not of type Annot, but of type:'+str(type(i))
		
		assert isinstance(linesLst, list), 'Error in Page() constuctor: linesLst must be of type list, and must contain instances of Line() objects'
		for i in linesLst:
			assert isinstance(i, Line), 'Error in Page() constuctor: linesLst has element which is not of type Line, but of type:'+str(type(i))
		
		assert isinstance(imgLst, list), 'Error in Page() constuctor: imgLst must be of type list, and must contain instances of Img() objects'
		for i in imgLst:
			assert isinstance(i, Img), 'Error in Page() constuctor: imgLst has element which is not of type Img, but of type:'+str(type(i))
		
		assert isinstance(size, list), 'Error in Page() constuctor: size must be of type list, and must contain 2 int values [w, h]'
		assert len(size)==0 or len(size)==2, 'Error in Page() constuctor: size must be of type list, and must be empty or contain 2 int values not '+str(len(size))+' values'
		for i in size:
			assert isinstance(i, int), 'Error in Page() constuctor: size has element which is not of type size, but of type:'+str(type(i))
		
		self.textLst = textLst
		self.fonts = []
		self.annots = annotLst
		self.lines = linesLst
		self.imgs = imgLst
		self.size = size
		
		for text in textLst:
			self.fonts.append(text.fontName)
			
		self.numberOfAnnots = len(annotLst)
		self.numberOfXObjects = len(imgLst)
		
	def getFonts(self):# returns list with names of fonts used on this Page
		return self.fonts
	def getTextLst(self):# returns list with Text-s used on this Page
		return self.textLst
		
	def getAnnotsList(self):# returns list of Annot objects which this page should display
		return self.annots
	def getNumberOfAnnots(self):
		return self.numberOfAnnots
		
	def getLinesList(self):# returns list of Img objects which this page should display
		return self.lines
		
	def getImgList(self):# returns list of Img objects which this page should display
		return self.imgs
	def getNumberOfXObjects(self):
		return self.numberOfXObjects
		
	def __str__(self):
		s = 'Page( '
		
		s += '[' + ', '.join( str(text) for text in self.textLst ) + '], '
		s += '[' + ', '.join( str(anot) for anot in self.annots ) + '], '
		s += '[' + ', '.join( str(line) for line in self.lines ) + '], '
		s += '[' + ', '.join( str(img) for img in self.imgs ) + '], '
		if self.size != []: s += '[' + ', '.join( str(i) for i in self.size ) + ']'
		
		return s + ' )'


'''
createPages writting structure

v--- Pages
|
|     Font objects <-----+------
|                                ^      |
+-->Page object[n] >=+      |
|                                V      |
|     text stream[n] <----       |
|                                        |
--->Page object[n+1] >=+--^
                                    |
      text stream[n+1] <---
	  .
	  .after last text stream
	  .
      Annot objects - referenced in Page object[n]
      XObject objects - referenced in Page object[n]
	  
	  Annot objects - referenced in Page object[n+1]
      XObject objects - referenced in Page object[n+1]

elements are written in order above into pdf file
arrows show what is referencing what
'''
# FreeObject will not add anything and have index 0, Catalog is at index 1 and should point to index 2(ie. Pages).
# Then Pages at index 2 points to first free index after indexes for Font objects which start at index 3(ie. first Font)
# Font at index 3 points to its parent(ie. index 2), same for any Font object that comes after it
# first Page object comes after last Font object
# Text stream object follows every Page object
'''
# this is simplest lst_of_objects structure that would be returned
lst_of_objects = [WriteFreeObject(fp),
						WriteCatalogObject(fp, 2),
						WritePagesObject(fp, [3], 600, 525),
						WriteFontObject(fp, 'Times-Italic'),
						WritePageObject(fp, 2, 5, [3], 6, 0, 6, 0, 'Im'),
						WriteTextObject(fp, (3, (20, 'text1', 0, 0)), [])]# (fontInx, Text(size, text, x, y,...)), []
'''
def createPages(fp, pageLst, sizeW, sizeH):

	assert isinstance(pageLst, list), 'Error in createPages(pageLst): pageLst must be of type list, and must contain instances of Page() objects'

	lst_of_objects = [ WriteFreeObject(fp), WriteCatalogObject(fp, 2) ]

	lstOfUTF8charsThatNeedToBeSubbed = []# list of multibyte codepoints
	fontsUsed = []# list of unique fonts
	for page in pageLst:
		assert isinstance(page, Page), 'Error: page passed to createPages not of type Page'
		# find lstOfUTF8charsThatNeedToBeSubbed
		for text in page.getTextLst():
			for c in text.text:
				if ord(c) <= int('0xA0', 16): continue# skip basic ASCII chars
				if c in lstOfUTF8charsThatNeedToBeSubbed: continue# add utf-8 chars only once
				lstOfUTF8charsThatNeedToBeSubbed.append(c)
				# FIXME lstOfUTF8charsThatNeedToBeSubbed gathers all utf-8 chars regardless of what Font that Text is using
		# calc numberOfPagesNeededForFonts
		for font in page.getFonts():
			if font in fontsUsed: pass
			else: fontsUsed.append(font)
	
	
	numberOfPagesNeededForFonts = len(fontsUsed)
	
	# make room for Font objects and calculate indexes of Page objects
	kidsIndexes = [i for i in range(3+numberOfPagesNeededForFonts, 2*len(pageLst)+3+numberOfPagesNeededForFonts, 2)]
	lst_of_objects.append( WritePagesObject(fp, kidsIndexes, sizeW, sizeH) )
	assert len(pageLst) == len(kidsIndexes), 'kidsIndexes for Page-s objects not calculated correctly'
	
	
	
	annotIndex = kidsIndexes[-1] + 1 + 1# after index of last Page and its assosiated contentStream is index of first Annot obj
	
	fontsUsedIndex = []# obj index given when writting Font objects
	i = 3# start counting after WriteFreeObject(not written but has index 0), WriteCatalogObject(written with index 1) and WritePagesObject(written with index 2)
	for fontName in fontsUsed:
		
		lst_of_objects.append( WriteFontObject(fp, fontName, lstOfUTF8charsThatNeedToBeSubbed) )
		# FIXME only give lstOfUTF8charsThatNeedToBeSubbed to Font-s that are being used to represent utf-8 chars
		fontsUsedIndex.append(i)
		i += 1
		
	for page, index in zip(pageLst, kidsIndexes):
	
		parentIndex = 2# Pages index
		contentStreamIndex = index+1
		fontIndexLst = [ fontsUsedIndex[fontsUsed.index(font)] for font in page.getFonts() ]
		
		firstAnnotIndex = annotIndex
		annotIndex += page.getNumberOfAnnots()# increment annotIndex, so on next page firstAnnotIndex gets correct value
		firstXObjectIndex = annotIndex# annotIndex is here index after last annot obj
		annotIndex += page.getNumberOfXObjects()
		
		lst_of_objects.append( WritePageObject(fp,
																	parentIndex, 
																	contentStreamIndex,
																	fontIndexLst,
																	firstAnnotIndex,
																	page.getNumberOfAnnots(),
																	firstXObjectIndex,
																	page.getNumberOfXObjects(),
																	Img('',0,0,0,0).pdfRefName,
																	page.size
																	)
										)
		
		
		textLstWithIndexToFonts = []
		
		for t in page.textLst:
			substituteIndex = 128# extended ASCII codepoint to write in Text insted of lstOfUTF8charsThatNeedToBeSubbed characters(which are multibyte)
			for c in lstOfUTF8charsThatNeedToBeSubbed:
				t.text = t.text.replace(c, chr(substituteIndex))
				substituteIndex += 1
				assert substituteIndex <= 255, 'substituteIndex for char ('+c+') exceded 255. Too many utf-8 chars.'
		
			fontInx = fontsUsed.index(t.fontName)
			txt = fontsUsedIndex[fontInx], t
			textLstWithIndexToFonts.append(txt)
		
		lst_of_objects.append( WriteTextObject(fp, textLstWithIndexToFonts, page.getLinesList(), page.getImgList()) )
	
	
	for page in pageLst:
		
		# add Annot objects for page
		# first Annot gets added at obj index: kidsIndexes[-1] + 1 + 1
		for annot in page.getAnnotsList():
			lst_of_objects.append( WriteAnnotObject(fp, annot) )
		
		
		# add XObject objects for page
		# first XObject gets added at obj index: kidsIndexes[-1] + 1 + 1 + indexOfLastAnnot
		for img in page.getImgList():
			
			lst_of_objects.append( WriteXObject(fp, img) )
	
	
	return lst_of_objects
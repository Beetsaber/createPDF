from writeToFile import writeToFile
from objectsWriting import WriteFreeObject


# lst_of_objects should contain all objects, but objects of type WriteFreeObject will not be added to the table
def cross_reference_tbl(fp, addresses_of_objs, lst_of_objects):

	writeToFile(fp, 'xref' + '\n' + '0 ' + str(len(lst_of_objects)))
		
	for addres, obj in zip(addresses_of_objs, lst_of_objects):
		
		adrStr = str(addres)
		
		# pad with 0's in order to have 10 character long number
		for fill0 in range(10 - len(adrStr)):
			adrStr = '0' + adrStr[:]
			
			
		generation = '00000'
		
		if isinstance(obj, WriteFreeObject):
			writeToFile(fp, '0000000000' + ' ' + '65535' + ' f')#f(free)
		else:
			writeToFile(fp, adrStr + ' ' + generation + ' n')#n(in-use) or f(free)


def trailer(fp, number_of_entries_in_cross_reference_table, catalog_dictionary, byte_offset_of_last_cross_reference_section):
	
	writeToFile(fp, 'trailer')
	writeToFile(fp, '<<')
	writeToFile(fp, '/Size ' + str(number_of_entries_in_cross_reference_table))
	writeToFile(fp, '/Root ' + catalog_dictionary)
	writeToFile(fp, '>>')

	writeToFile(fp, 'startxref')
	writeToFile(fp, str(byte_offset_of_last_cross_reference_section))
	writeToFile(fp, '%%EOF')
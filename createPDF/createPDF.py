#!/usr/bin/python
# -*- coding: UTF-8 -*-
#GNU Affero General Public License
'''

https://www.adobe.com/content/dam/acom/en/devnet/pdf/PDF32000_2008.pdf

PDF files contain 4 parts:

    the header
    the document body, containing at least 3 indirect objects
    the xref cross-reference table
    the trailer (including startxref and %%EOF).

'''

from writeToFile import writeToFile
from objectsWriting import *
from referencesWriting import *


def header(fp, s):
	headerSize = writeToFile(fp, s)
	# as per spec there should be 4 chars with values >=128
	headerSize += writeToFile(fp, '%\x80\x80')
	return headerSize


'''
# Link to website in bottom left corner of second page
def getPagesList():
	# Bottom left corner has coordinates 0,0
	
	# Page format; more detailed object stuctures information at start of objectsWriting.py file
	# Page(textLst=[Text(),Text(),...], annotLst=[Annot(),Annot(),...], linesLst=[Line(),Line(),...], imgLst=[Img(),Img(),...], size=[w,h])
	
	t1 = Text('Courier-Bold', 12, 'text1', 520, 440)
	t2 = Text('Helvetica-Oblique', 30, 'text2', 120, 220)
	t3 = Text('Times-Italic', 40, 'text3', 120, 140)
	pagesList = [  Page( [t1,], [] ), Page( [t2, t3], [Annot('https://www.google.com', 0, 0, 300, 300),] )  ]
	
	return pagesList
'''

'''
# Image on screen
def getPagesList():
	
	t1 = Text('Courier-Bold', 12, 'text1', 520, 440)
	t2 = Text('Helvetica-Oblique', 30, 'text2', 120, 220, '1.00 0.00 0.30')
	t3 = Text('Times-Italic', 40, 'text3', 120, 140)
	#pagesList = [  Page( [t1, t2, t3], [], [], [Img('examples/Im1.jpeg', 0, 0, 400, 400)] )  ]
	
	p2Width, p2Height = 270, 370
	pagesList = [  Page( [t1, t2, t3], [], [Line(5, 30,400,900,400), Line(5, 30,405,900,405, '0.50 0.40 0.70')], [Img('examples/Im1.jpeg', 100, 200, 600, 525)] ),\
						Page( [t1, t2, t3], [], [], [Img('examples/Im1.jpeg', 100, 200, 600, 525)], [p2Width, p2Height] )  ]
	
	#from PIL import Image
	#pagesList = [  Page([t1],[],[], [Img('examples/Im2.jpeg', 50, 50, Image.open('examples/Im2.jpeg').width, Image.open('examples/Im2.jpeg').height, 2,2)]),\
	#						Page([t1],[],[], [Img('examples/Im2.jpeg', 50, 50, Image.open('examples/Im2.jpeg').width, Image.open('examples/Im2.jpeg').height)]),\
	#						Page([],[],[], [Img('examples/Im2.jpeg', 100, 200, 65, 25)] ),\
	#						Page([],[],[], [Img('examples/Im2.jpeg', 100, 200, 65, 25,6,6),Img('examples/Im2.jpeg', 500, 200, 65, 25,6,6)] )  ]
	#Note: some browsers may not scale image in same way which is not scalled with scale params but which simply has its w and h changed
	
	return pagesList
'''


def getPagesList():
	# Only 'Verdana' font supports utf-8 chars via in-build /Differences list; Maximum 128 utf-8 chars allowed
	
	x, y = 50, 600
	t1 = Text('Verdana', 30, 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz', x, y)
	t2 = Text('Verdana', 30, 'Если вы любите кататься на санках, вам должно нравиться тащить сани.', x, y-100)
	t3 = Text('Verdana', 30, 'ओ औ ऑ क ख ग घ ङ च छ ज झ ञ ट ठ ड ढ ण त थ द ध न ऩ', x, y-200)
	t4 = Text('Verdana', 30, 'كلمات عربية', x, y-300)
	#import string;t4 = Text('Verdana', 20, string.printable, x, y-300)
	t5 = Text('Verdana', 30, 'Σ 	Τ 	Υ 	Φ 	Χ 	Ψ 	Ω', x, y-400)
	#Մի ապուշ քարը գցեց ջրհորը, քառասուն խելոք չկարողացան այն հանել։ language doesnt work on all browsers
	#after utf-8 block 2 languages don't work (ex. 新的文本阅读器')
	
	pagesList = [  Page( [ t1, t2, t3, t4, t5 ], [], [], [] )  ]
	
	return pagesList
	

# if pagesList is not provided getPagesList() will be called to fill pagesList
def createPDF(outputFileName, w, h, pagesList=[]):
	
	with open(outputFileName, 'wb') as fp:
		
		# header
		headerSize = header(fp, '%PDF-1.1')

		
		# possible fonts; or use Verdana for utf-8 text; if utf-8 text is given WriteFontObject will switch to Verdana (overwriting provided font)
		standard14Fonts = ['Times-Roman', 'Helvetica', 'Courier', 'Symbol', 'Times-Bold', 'Helvetica-Bold',
									'Courier-Bold', 'ZapfDingbats', 'Times-Italic', 'Helvetica-Oblique', 'Courier-Oblique',
									'Times-BoldItalic', 'Helvetica-BoldOblique', 'Courier-BoldOblique']
		
		if pagesList == []:
			pagesList = getPagesList()# this function defines everything that is placed on screen
		
		# get list of objects
		lst_of_objects = createPages( fp, pagesList, w, h )# implicit in this call Structual pages(Free, Catalog, Pages) will be created
		
		# write objects(obj) to file
		addresses_of_objs, last_object_size = objects(fp, headerSize, lst_of_objects)
		

		# cross-reference table
		cross_reference_tbl(fp, addresses_of_objs, lst_of_objects)
		
		
		# trailer part
		number_of_entries_in_cross_reference_table = len(lst_of_objects)
		catalog_dictionary = '1 0 R'
		byte_offset_of_last_cross_reference_section = addresses_of_objs[-1]+last_object_size# In decimal address of xref.
		
		trailer(fp, number_of_entries_in_cross_reference_table, catalog_dictionary, byte_offset_of_last_cross_reference_section)
		
		print('\n\nCode used to create pdf:\npagesList = [', end='\n\t')
		print(',\n\t'.join(str(o) for o in pagesList))
		print(']')
		

if __name__ == '__main__':

	outputFileName = 'file.pdf'
	w, h = 1300, 800

	#verbose printing can be switched on in writeToFile
	createPDF(outputFileName, w, h)
		
